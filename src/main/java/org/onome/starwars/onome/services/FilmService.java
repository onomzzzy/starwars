package org.onome.starwars.onome.services;

import java.util.ArrayList;
import java.util.List;

import org.onome.starwars.onome.model.Film;
import org.onome.starwars.onome.model.Result;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.MediaType;

public class FilmService {
	public Film getFilms() {
		Client client = ClientBuilder.newClient();
		Film films = client.target("https://onome.s3.us-east-2.amazonaws.com/starwar.json")
		.request(MediaType.APPLICATION_JSON)
		.get(Film.class);
		//.get(new GenericType<List<Object>>() {});
		return films;
	}
	
	
	public Film getFilm(String id) {
		Client client = ClientBuilder.newClient();
		Film films = client.target("https://onome.s3.us-east-2.amazonaws.com/starwar.json")
		.request(MediaType.APPLICATION_JSON)
		.get(Film.class);
		Film film = null;
		//.get(new GenericType<List<Object>>() {});
		List <Result> oldResults = films.getResults();
		List<Result> newResult = new ArrayList<>();
		for (Result i : oldResults ) 
		{ 
		    if(i.getUrl().contains(id)) {
		    	newResult.add(i);
		    	film = films;
		    	film.setResults(newResult);
		    	break;
		    }
		}
		return film;
	}
}
