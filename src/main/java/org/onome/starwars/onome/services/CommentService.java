package org.onome.starwars.onome.services;


//import java.io.File;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import org.onome.starwars.onome.model.Comment;
import org.onome.starwars.onome.model.Key;
//import org.onome.starwars.onome.model.Key;
import org.onome.starwars.onome.model.MovieComment;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
//import com.amazonaws.services.s3.model.CannedAccessControlList;
//import com.amazonaws.services.s3.model.PutObjectRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.MediaType;

public class CommentService {
	//Regions clientRegion = Regions.DEFAULT_REGION;
    String bucketName = "onome";
    String stringObjKeyName = "comment.json";
	


public MovieComment getComments() {
	Client client = ClientBuilder.newClient();
	MovieComment comments = client.target("https://onome.s3.us-east-2.amazonaws.com/comment.json")
	.request(MediaType.APPLICATION_JSON)
	.get(MovieComment.class);
	return comments;
}


public List<Comment> getFilmComments(String movieId) {
	MovieComment comments = this.getComments();
	List<Comment> old = comments.getComments();
	List<Comment> filmComment = new ArrayList<>();
	for (Comment i : old ) 
	{ 
	    if(i.getMovieId().equals(movieId)) {
	    	filmComment.add(i);
	    }
	}
	return filmComment;
}

public List<Comment> getFilmCommentsByCommentId(String cId) {
	MovieComment comments = this.getComments();
	List<Comment> old = comments.getComments();
	List<Comment> filmComment = new ArrayList<>();
	for (Comment i : old ) 
	{ 
	    if(i.getId().equals(cId)) {
	    	filmComment.add(i);
	    }
	}
	return filmComment;
}

public void setComment(Comment comment,String filmId) {
	Key key = new Key();
	MovieComment commentz = this.getComments();
	List<Comment> comments = commentz.getComments();
	//Set Id, filmId, Date
	Instant instant = Instant.now();
	comment.setId(instant.getEpochSecond()+"");
	comment.setMovieId(filmId);
	comment.setDate(instant.toString());
	comments.add(comment);
	 //Creating the ObjectMapper object
    ObjectMapper mapper = new ObjectMapper();
    //Converting the Object to JSONString
    try {
		String jsonString = mapper.writeValueAsString(comments);
		 // Upload a text string as a new object.
		AWSCredentials credentials = new BasicAWSCredentials(
				  key.getId(), 
             key.getPwd()
				);
		
		AmazonS3 s3Client = AmazonS3ClientBuilder
				  .standard()
				  .withCredentials(new AWSStaticCredentialsProvider(credentials))
				  .withRegion(Regions.US_EAST_2)
				  .build();
		
		 /*PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, stringObjKeyName, jsonString);
         putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead); // public for all
         s3Client.putObject(putObjectRequest);*/ // upload file
		s3Client.putObject(
				  bucketName,
				  stringObjKeyName,
				  jsonString
				);
	} catch (JsonProcessingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
}
