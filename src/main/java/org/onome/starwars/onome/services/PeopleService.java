package org.onome.starwars.onome.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.onome.starwars.onome.model.People;
import org.onome.starwars.onome.model.extra.Result;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.MediaType;

public class PeopleService {
	
	public People getPeople() {
		Client client = ClientBuilder.newClient();
		People people = client.target("https://onome.s3.us-east-2.amazonaws.com/people.json")
		.request(MediaType.APPLICATION_JSON)
		.get(People.class);
		//.get(new GenericType<List<Object>>() {});
		return people;
	}
	
	
	public People getPersonByName(String name) {
		People people = this.getPeople();
		People person = null;
		//.get(new GenericType<List<Object>>() {});
		List <Result> oldResults = people.getResults();
		List<Result> newResult = new ArrayList<>();
		for (Result i : oldResults ) 
		{ 
		    if(i.getName().equalsIgnoreCase(name)) {
		    	newResult.add(i);
		    	person = people;
		    	person.setResults(newResult);
		    	break;
		    }
		}
		return person;
	}
	

	public People getPersonByHeight(String height) {
		People people = this.getPeople();
		People persons = null;
		//.get(new GenericType<List<Object>>() {});
		List <Result> oldResults = people.getResults();
		List<Result> newResult = new ArrayList<>();
		for (Result i : oldResults ) 
		{ 
		    if(i.getHeight().equals(height)) {
		    	newResult.add(i);
		    }
		}
		//Sort List
		Collections.sort(newResult); 
		//set new people
		persons = people;
    	persons.setResults(newResult);
		return persons;
	}
	
	
	public People getPersonByGender(String gender) {
		People people = this.getPeople();
		People persons = null;
		//.get(new GenericType<List<Object>>() {});
		List <Result> oldResults = people.getResults();
		List<Result> newResult = new ArrayList<>();
		for (Result i : oldResults ) 
		{ 
		    if(i.getGender().equals(gender)) {
		    	newResult.add(i);
		    }
		}
		//set new people
		persons = people;
    	persons.setResults(newResult);
		return persons;
	}
	
}
