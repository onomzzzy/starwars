package org.onome.starwars.onome.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class MovieComment {
	
	
public MovieComment() {

	}

private int count;
private List<Comment> comments;

public int getCount() {
	return count;
}
public void setCount(int count) {
	this.count = count;
}
public List<Comment> getComments() {
	return comments;
}
public void setComments(List<Comment> comments) {
	this.comments = comments;
}


}
