package org.onome.starwars.onome.model.extra;
import java.util.List;

import weka.core.Utils;

public class Result implements Comparable< Result >{
	private String name;
    private String height;
    private String totalHeight;
    private String mass;
    private Integer id;
    private String hair_color;
    private String skin_color;
    private String eye_color;
    private String birth_year;
    private String gender;
   // private String homeworld;
    private List<String> films;
   // private List<String> species;
   // private List<String> starships;
    private String created;
    private String edited;
    private String url;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
		this.id = (Integer.parseInt(height));
		double heightInCm = Double.parseDouble(height);
		int heightInFeet = (int) Math.ceil( (heightInCm * 0.0328084));
		double heightInInches = Utils.roundDouble((heightInCm * 0.393701), 2);
		String totalHeight = ((int)(heightInCm))+"cm "+heightInFeet+"ft "+ heightInInches+" inches";
		this.setTotalHeight(totalHeight);
	}
	public String getMass() {
		return mass;
	}
	public void setMass(String mass) {
		this.mass = mass;
	}
	public String getHair_color() {
		return hair_color;
	}
	public void setHair_color(String hair_color) {
		this.hair_color = hair_color;
	}
	public String getSkin_color() {
		return skin_color;
	}
	public void setSkin_color(String skin_color) {
		this.skin_color = skin_color;
	}
	public String getEye_color() {
		return eye_color;
	}
	public void setEye_color(String eye_color) {
		this.eye_color = eye_color;
	}
	public String getBirth_year() {
		return birth_year;
	}
	public void setBirth_year(String birth_year) {
		this.birth_year = birth_year;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	/*public String getHomeworld() {
		return homeworld;
	}
	public void setHomeworld(String homeworld) {
		this.homeworld = homeworld;
	}
	
	public List<String> getSpecies() {
		return species;
	}
	public void setSpecies(List<String> species) {
		this.species = species;
	}
	public List<String> getStarships() {
		return starships;
	}
	public void setStarships(List<String> starships) {
		this.starships = starships;
	}*/
	
	public List<String> getFilms() {
		return films;
	}
	public void setFilms(List<String> films) {
		this.films = films;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getEdited() {
		return edited;
	}
	public void setEdited(String edited) {
		this.edited = edited;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTotalHeight() {
		return totalHeight;
	}
	public void setTotalHeight(String totalHeight) {
		this.totalHeight = totalHeight;
	}
    
	 @Override
	    public int compareTo(Result o) {
	        return this.getId().compareTo(o.getId());
	    }
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
    
    
}
