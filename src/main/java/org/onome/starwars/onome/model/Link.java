package org.onome.starwars.onome.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//import org.onome.starwars.onome.resources.CommentResource;
import org.onome.starwars.onome.resources.FilmResource;
import org.onome.starwars.onome.resources.PeopleResource;
import org.onome.starwars.onome.services.CommentService;

import jakarta.ws.rs.core.UriInfo;

public class Link {
	public Film changeLinks(Film films, List<Result> results, UriInfo uriInfo) {
		Film result = films;
		List<String> character;
		CommentService cs = new CommentService();
		
		for(int i=0;i<results.size();i++) {
			
			
			//get Character
			character = new ArrayList<>();
			List<String> characters = results.get(i).getCharacters();
			for(int j =0;j<characters.size();j++) {
				String [] k = characters.get(j).split("/");
				String id = k[k.length -1];
				String uri = uriInfo.getBaseUriBuilder()
						.path(PeopleResource.class)
						.path(id)
						.build()
						.toString();
				character.add(uri);
			}
	
			
			String [] k = result.getResults().get(i).getUrl().split("/");
			String id = k[k.length -1];
			//get comment links
			List<Comment> cm = cs.getFilmComments(id);
			cm = this.setLinkComment(cm, uriInfo);
			//set Film Comments
			List<String> cmms = new ArrayList<>();
			for(int z=0;z<cm.size();z++) {
				cmms.add(cm.get(z).getUri());
			}
			result.getResults().get(i).setComments(cmms);
			
			//Set Self URI
			String uri = uriInfo.getBaseUriBuilder()
					.path(FilmResource.class)
					.path(id)
					.build()
					.toString();
			//character.add(uri);
			//Add new Links
			result.getResults().get(i).setCharacters(character);
			result.getResults().get(i).setUrl(uri);
			
		}

		
		return result;
	}
	
	public List<Comment> setLinkComment(List<Comment> cm, UriInfo uriInfo) {
		List<Comment> comments  = cm;
		for(int i=0;i<comments.size();i++) {
			//get Character
				String uri = uriInfo.getBaseUriBuilder()
						.path(FilmResource.class)
						.path(cm.get(i).getMovieId())
						.path("comments")
						.path(cm.get(i).getId())
						.build()
						.toString();
				comments.get(i).setUri(uri);
		
	
		}
		Collections.reverse(comments);
	
		return comments;
	}
	
	public People changeLinksPeople(People people, List< org.onome.starwars.onome.model.extra.Result> results, UriInfo uriInfo) {
		People result = people;
		List<String> film;
		for(int i=0;i<results.size();i++) {
			//get Films
			film = new ArrayList<>();
			List<String> films = results.get(i).getFilms();
			for(int j =0;j<films.size();j++) {
				String [] k = films.get(j).split("/");
				String id = k[k.length -1];
				String uri = uriInfo.getBaseUriBuilder()
						.path(FilmResource.class)
						.path(id)
						.build()
						.toString();
				film.add(uri);
			}
	
			//Set Self URI
			String [] k = result.getResults().get(i).getUrl().split("/");
			String id = k[k.length -1];
			String uri = uriInfo.getBaseUriBuilder()
					.path(PeopleResource.class)
					.path(id)
					.build()
					.toString();
			//character.add(uri);
			//Add new Links
			result.getResults().get(i).setFilms(film);
			result.getResults().get(i).setUrl(uri);
			
		}

		
		return result;
	}
}
