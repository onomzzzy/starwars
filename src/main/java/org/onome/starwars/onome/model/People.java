package org.onome.starwars.onome.model;


import java.util.List;
import org.onome.starwars.onome.model.extra.Result;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class People {
	private int count; 
    //private String next;
   // private String previous;
	private List<Result> results;
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	/*public String getNext() {
		return next;
	}
	public void setNext(String next) {
		this.next = next;
	}
	public String getPrevious() {
		return previous;
	}
	public void setPrevious(String previous) {
		this.previous = previous;
	}*/
	public List<Result> getResults() {
		return results;
	}
	public void setResults(List<Result> results) {
		this.results = results;
	} 
	
}

