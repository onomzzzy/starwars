package org.onome.starwars.onome.resources;


import java.util.List;

import org.onome.starwars.onome.model.Comment;
import org.onome.starwars.onome.model.Link;
//import org.onome.starwars.onome.model.MovieComment;
import org.onome.starwars.onome.services.CommentService;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
//import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.UriInfo;

@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CommentResource {
private CommentService cs = new CommentService();
	
@GET	
public List<Comment> getComments(@PathParam("filmId") String filmId,@Context UriInfo uriInfo) {
	Link link = new Link();
	return link.setLinkComment(cs.getFilmComments(filmId), uriInfo);
	
}

@GET
@Path("/show")
public String getCommentz(@PathParam("filmId") String filmId,@Context UriInfo uriInfo) {
	return "Yeah yeah";
	
}



@GET
@Path("{commentId}")
public List<Comment> getComment(@PathParam("commentId") String commentId,@Context UriInfo uriInfo) {
	Link link = new Link();
	return link.setLinkComment(cs.getFilmCommentsByCommentId(commentId), uriInfo);
}


@PUT
public void postComment(@PathParam("filmId")  String filmId, Comment comment) {
	//Put Call No Fixed yet
	//Problem with My Writing to my S3 (AWS)
	//Please for references only
	//Check Comment Service for implementation
	//cs.setComment(comment,filmId);
}

}
