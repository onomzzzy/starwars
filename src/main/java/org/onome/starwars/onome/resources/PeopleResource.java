package org.onome.starwars.onome.resources;
import org.onome.starwars.onome.model.Link;
import org.onome.starwars.onome.model.People;
import org.onome.starwars.onome.services.PeopleService;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.UriInfo;

@Path("people")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PeopleResource {

	private PeopleService pservice = new PeopleService();

	@GET
	public People getPeople(@Context UriInfo uriInfo) {
	People people = this.pservice.getPeople();
	//Change Link 
	Link link = new Link();
	people = link.changeLinksPeople(people,people.getResults(),uriInfo);		
	return people;
	}
	
	@GET
	@Path("gender/{gender}")
	public People getPeopleByGender(@PathParam("gender") String gender,@Context UriInfo uriInfo) {
	People people = this.pservice.getPersonByGender(gender);
	//Change Link 
	Link link = new Link();
	people = link.changeLinksPeople(people,people.getResults(),uriInfo);		
	return people;
	}
	
	@GET
	@Path("height/{height}")
	public People getPeopleByHeight(@PathParam("height") String height,@Context UriInfo uriInfo) {
	People people = this.pservice.getPersonByHeight(height);
	//Change Link 
	Link link = new Link();
	people = link.changeLinksPeople(people,people.getResults(),uriInfo);		
	return people;
	}
	
	@GET
	@Path("name/{name}")
	public People getPeopleByName(@PathParam("name") String name,@Context UriInfo uriInfo) {
	People people = this.pservice.getPersonByName(name);
	//Change Link 
	Link link = new Link();
	people = link.changeLinksPeople(people,people.getResults(),uriInfo);		
	return people;
	}
}

