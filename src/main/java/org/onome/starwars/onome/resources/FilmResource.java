package org.onome.starwars.onome.resources;

import org.onome.starwars.onome.model.Film;
import org.onome.starwars.onome.model.Link;
import org.onome.starwars.onome.services.FilmService;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.UriInfo;

	@Path("movies")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public class FilmResource {
		
		private FilmService service = new FilmService();

		@GET
		public Film getFilms(@Context UriInfo uriInfo) {
		Film films = this.service.getFilms();
		//Change Link 
		Link link = new Link();
		
		films = link.changeLinks(films,films.getResults(),uriInfo);		
		return films;
		}
		

		
		@GET
		@Path("/{filmId}")
		public Film getFilm(@PathParam("filmId") String filmId,@Context UriInfo uriInfo) {
		Film film = this.service.getFilm(filmId);
		System.out.println(film);
		//Change Link 
		if(film!=null) {
		Link link = new Link();
		film = link.changeLinks(film,film.getResults(),uriInfo);
		}
		
		return film;
		}
		
		
		@Path("/{filmId}/comments")
		public CommentResource getCommentResource(@PathParam("filmId") String filmId,@Context UriInfo uriInfo) {
		return new CommentResource();
		}
	
}
